﻿using System.Collections.Generic;

namespace MyLibrary
{
    public class ComplexType
    {
        public string Name { get; set; }
        public SimpleType Data { get; set; }
        public List<SimpleType> MoreData { get; set; }
    }
}
