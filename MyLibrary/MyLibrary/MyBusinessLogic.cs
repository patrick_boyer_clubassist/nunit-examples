﻿namespace MyLibrary
{
    public class MyBusinessLogic
    {
        private SimpleType SimpleState;
        private ComplexType ComplexState;

        public MyBusinessLogic(SimpleType simpleState, ComplexType complexState)
        {
            SimpleState = simpleState;
            ComplexState = complexState;
        }

        public bool DoAnyContain(string data)
        {
            foreach (SimpleType s in ComplexState.MoreData)
            {
                if (s.Data == data)
                {
                    return true;
                }
            }
            return SimpleState.Data == data;
        }

        public bool DoesFullStateContain(SimpleType data)
        {
            foreach (SimpleType s in ComplexState.MoreData)
            {
                if (s.Id == data.Id)
                {
                    return true;
                }
            }

            return (ComplexState.Data.Id == data.Id);


        }

        public bool IsTheComplexState(ComplexType state)
        {
            return state.Name == ComplexState.Name;
        }

        public ComplexType AddToComplexState(SimpleType s)
        {
            ComplexState.MoreData.Add(s);
            return ComplexState;
        }

        public ComplexType AddToComplexState(SimpleType one, SimpleType two, SimpleType three)
        {
            ComplexState.MoreData.Add(one);
            ComplexState.MoreData.Add(two);
            ComplexState.MoreData.Add(three);
            return ComplexState;
        }

        public ComplexType AppendToComplexState(ComplexType one, ComplexType two)
        {
            foreach (SimpleType s in one.MoreData)
            {
                ComplexState.MoreData.Add(s);
            }

            foreach (SimpleType s in two.MoreData)
            {
                ComplexState.MoreData.Add(s);
            }

            SimpleState = two.Data;
            return ComplexState;
        }
    }
}
