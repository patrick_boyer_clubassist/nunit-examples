﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

namespace MyLibrary.NUnitTests
{
    public class TestData
    {
        public static IEnumerable Names
        {
            get
            {
                yield return new TestCaseData(null).Returns(false);
                yield return new TestCaseData(string.Empty).Returns(false);
                yield return new TestCaseData("Hello").Returns(true);
                yield return new TestCaseData("Goodbye").Returns(false);
            }
        }

        public static IEnumerable SimpleStates
        {
            get
            {
                yield return new TestCaseData(null).Returns(false);
                yield return new TestCaseData(s1).Returns(true);
                yield return new TestCaseData(s2).Returns(true);
            }
        }

        public static IEnumerable SimpleStatesToAdd
        {
            get
            {
                yield return new TestCaseData(null).Returns(0);
                yield return new TestCaseData(s1).Returns(1);
                yield return new TestCaseData(s2).Returns(1);
            }
        }

        public static IEnumerable SimpleStateTriplets
        {
            get
            {
                yield return new TestCaseData(null, s1, s2).Returns(2);
                yield return new TestCaseData(s1, s1, s1).Returns(1);
                yield return new TestCaseData(s1, s2, s3).Returns(3);
            }
        }

        public static IEnumerable ComplexStatePairs
        {
            get
            {
                yield return new TestCaseData(null, null).Returns(0);
                yield return new TestCaseData(c, c).Returns(1);
                yield return new TestCaseData(c, c2).Returns(2);
                yield return new TestCaseData(c2, c).Returns(2);
                yield return new TestCaseData(c2, c3).Returns(2);
            }
        }

        private static SimpleType s1 = new SimpleType()
        {
            Id = 1,
            Data = "Hello"
        };

        private static SimpleType s2 = new SimpleType()
        {
            Id = 2,
            Data = "World"
        };

        private static SimpleType s3 = new SimpleType()
        {
            Id = 3,
            Data = "Three"
        };

        private static ComplexType c = new ComplexType()
        {
            Data = s1,
            MoreData = new List<SimpleType>(),
            Name = "The name"
        };

        private static ComplexType c2 = new ComplexType()
        {
            Data = s2,
            MoreData = new List<SimpleType>(),
            Name = "C2"
        };

        private static ComplexType c3 = new ComplexType()
        {
            Data = s3,
            MoreData = new List<SimpleType>(),
            Name = "C3"
        };
    }
}
