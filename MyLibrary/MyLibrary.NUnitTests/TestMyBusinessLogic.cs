﻿using NUnit.Framework;
using System.Collections.Generic;

namespace MyLibrary.NUnitTests
{
    [TestFixture]
    public class TestMyBusinessLogic
    {
        private MyBusinessLogic sut;

        [SetUp]
        public void Setup()
        {
            SimpleType s = new SimpleType()
            {
                Id = 1,
                Data = "Hello"
            };

            ComplexType c = new ComplexType()
            {
                Data = s,
                MoreData = new List<SimpleType>(),
                Name = "The name"
            };

            sut = new MyBusinessLogic(s, c);
        }

        [Test]
        public void TraditionalUnitTest()
        {
            bool actual = sut.DoAnyContain("Hello");
            bool expected = true;
            Assert.AreEqual(actual, expected);
        }

        [Test, TestCaseSource(typeof(TestData), "Names")]
        public bool TestDoesAnyContains(string input)
        {           
            return sut.DoAnyContain(input);
        }

        [Test, TestCaseSource(typeof(TestData), "SimpleStates")]
        public bool TestDoesFullStateContain(SimpleType s)
        {           
            return sut.DoesFullStateContain(s);
        }

        [Test, TestCaseSource(typeof(TestData), "SimpleStatesToAdd")]
        public int TestAddToComplexState(SimpleType s)
        {
            ComplexType result = sut.AddToComplexState(s);
            return result.MoreData.Count;
        }

        [Test, TestCaseSource(typeof(TestData), "SimpleStateTriplets")]
        public int TestAddThreeToComplexState(SimpleType s1, SimpleType s2, SimpleType s3)
        {
            ComplexType result = sut.AddToComplexState(s1, s2, s3);
            return result.MoreData.Count;
        }

        [Test, TestCaseSource(typeof(TestData), "ComplexStatePairs")]
        public int TestAppendToComplexState(ComplexType c1, ComplexType c2)
        {
            ComplexType result = sut.AppendToComplexState(c1, c2);
            return result.MoreData.Count;
        }
    }
}